// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OFFICEESCAPE_OfficeEscapeGameMode_generated_h
#error "OfficeEscapeGameMode.generated.h already included, missing '#pragma once' in OfficeEscapeGameMode.h"
#endif
#define OFFICEESCAPE_OfficeEscapeGameMode_generated_h

#define OfficeEscape_Source_OfficeEscape_OfficeEscapeGameMode_h_12_RPC_WRAPPERS
#define OfficeEscape_Source_OfficeEscape_OfficeEscapeGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define OfficeEscape_Source_OfficeEscape_OfficeEscapeGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAOfficeEscapeGameMode(); \
	friend OFFICEESCAPE_API class UClass* Z_Construct_UClass_AOfficeEscapeGameMode(); \
public: \
	DECLARE_CLASS(AOfficeEscapeGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/OfficeEscape"), OFFICEESCAPE_API) \
	DECLARE_SERIALIZER(AOfficeEscapeGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define OfficeEscape_Source_OfficeEscape_OfficeEscapeGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAOfficeEscapeGameMode(); \
	friend OFFICEESCAPE_API class UClass* Z_Construct_UClass_AOfficeEscapeGameMode(); \
public: \
	DECLARE_CLASS(AOfficeEscapeGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/OfficeEscape"), OFFICEESCAPE_API) \
	DECLARE_SERIALIZER(AOfficeEscapeGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define OfficeEscape_Source_OfficeEscape_OfficeEscapeGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	OFFICEESCAPE_API AOfficeEscapeGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AOfficeEscapeGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(OFFICEESCAPE_API, AOfficeEscapeGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOfficeEscapeGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	OFFICEESCAPE_API AOfficeEscapeGameMode(AOfficeEscapeGameMode&&); \
	OFFICEESCAPE_API AOfficeEscapeGameMode(const AOfficeEscapeGameMode&); \
public:


#define OfficeEscape_Source_OfficeEscape_OfficeEscapeGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	OFFICEESCAPE_API AOfficeEscapeGameMode(AOfficeEscapeGameMode&&); \
	OFFICEESCAPE_API AOfficeEscapeGameMode(const AOfficeEscapeGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(OFFICEESCAPE_API, AOfficeEscapeGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOfficeEscapeGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AOfficeEscapeGameMode)


#define OfficeEscape_Source_OfficeEscape_OfficeEscapeGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define OfficeEscape_Source_OfficeEscape_OfficeEscapeGameMode_h_9_PROLOG
#define OfficeEscape_Source_OfficeEscape_OfficeEscapeGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	OfficeEscape_Source_OfficeEscape_OfficeEscapeGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	OfficeEscape_Source_OfficeEscape_OfficeEscapeGameMode_h_12_RPC_WRAPPERS \
	OfficeEscape_Source_OfficeEscape_OfficeEscapeGameMode_h_12_INCLASS \
	OfficeEscape_Source_OfficeEscape_OfficeEscapeGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define OfficeEscape_Source_OfficeEscape_OfficeEscapeGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	OfficeEscape_Source_OfficeEscape_OfficeEscapeGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	OfficeEscape_Source_OfficeEscape_OfficeEscapeGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	OfficeEscape_Source_OfficeEscape_OfficeEscapeGameMode_h_12_INCLASS_NO_PURE_DECLS \
	OfficeEscape_Source_OfficeEscape_OfficeEscapeGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID OfficeEscape_Source_OfficeEscape_OfficeEscapeGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
