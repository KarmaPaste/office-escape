// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "OfficeEscapeGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOfficeEscapeGameMode() {}
// Cross Module References
	OFFICEESCAPE_API UClass* Z_Construct_UClass_AOfficeEscapeGameMode_NoRegister();
	OFFICEESCAPE_API UClass* Z_Construct_UClass_AOfficeEscapeGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_OfficeEscape();
// End Cross Module References
	void AOfficeEscapeGameMode::StaticRegisterNativesAOfficeEscapeGameMode()
	{
	}
	UClass* Z_Construct_UClass_AOfficeEscapeGameMode_NoRegister()
	{
		return AOfficeEscapeGameMode::StaticClass();
	}
	UClass* Z_Construct_UClass_AOfficeEscapeGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AGameModeBase,
				(UObject* (*)())Z_Construct_UPackage__Script_OfficeEscape,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
				{ "IncludePath", "OfficeEscapeGameMode.h" },
				{ "ModuleRelativePath", "OfficeEscapeGameMode.h" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AOfficeEscapeGameMode>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AOfficeEscapeGameMode::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00880288u,
				nullptr, 0,
				nullptr, 0,
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AOfficeEscapeGameMode, 2186079651);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AOfficeEscapeGameMode(Z_Construct_UClass_AOfficeEscapeGameMode, &AOfficeEscapeGameMode::StaticClass, TEXT("/Script/OfficeEscape"), TEXT("AOfficeEscapeGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AOfficeEscapeGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
