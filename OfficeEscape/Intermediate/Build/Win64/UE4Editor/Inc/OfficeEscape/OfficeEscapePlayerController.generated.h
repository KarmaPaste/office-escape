// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OFFICEESCAPE_OfficeEscapePlayerController_generated_h
#error "OfficeEscapePlayerController.generated.h already included, missing '#pragma once' in OfficeEscapePlayerController.h"
#endif
#define OFFICEESCAPE_OfficeEscapePlayerController_generated_h

#define OfficeEscape_Source_OfficeEscape_OfficeEscapePlayerController_h_12_RPC_WRAPPERS
#define OfficeEscape_Source_OfficeEscape_OfficeEscapePlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define OfficeEscape_Source_OfficeEscape_OfficeEscapePlayerController_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAOfficeEscapePlayerController(); \
	friend OFFICEESCAPE_API class UClass* Z_Construct_UClass_AOfficeEscapePlayerController(); \
public: \
	DECLARE_CLASS(AOfficeEscapePlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), 0, TEXT("/Script/OfficeEscape"), NO_API) \
	DECLARE_SERIALIZER(AOfficeEscapePlayerController) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define OfficeEscape_Source_OfficeEscape_OfficeEscapePlayerController_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAOfficeEscapePlayerController(); \
	friend OFFICEESCAPE_API class UClass* Z_Construct_UClass_AOfficeEscapePlayerController(); \
public: \
	DECLARE_CLASS(AOfficeEscapePlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), 0, TEXT("/Script/OfficeEscape"), NO_API) \
	DECLARE_SERIALIZER(AOfficeEscapePlayerController) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define OfficeEscape_Source_OfficeEscape_OfficeEscapePlayerController_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AOfficeEscapePlayerController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AOfficeEscapePlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOfficeEscapePlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOfficeEscapePlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOfficeEscapePlayerController(AOfficeEscapePlayerController&&); \
	NO_API AOfficeEscapePlayerController(const AOfficeEscapePlayerController&); \
public:


#define OfficeEscape_Source_OfficeEscape_OfficeEscapePlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOfficeEscapePlayerController(AOfficeEscapePlayerController&&); \
	NO_API AOfficeEscapePlayerController(const AOfficeEscapePlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOfficeEscapePlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOfficeEscapePlayerController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AOfficeEscapePlayerController)


#define OfficeEscape_Source_OfficeEscape_OfficeEscapePlayerController_h_12_PRIVATE_PROPERTY_OFFSET
#define OfficeEscape_Source_OfficeEscape_OfficeEscapePlayerController_h_9_PROLOG
#define OfficeEscape_Source_OfficeEscape_OfficeEscapePlayerController_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	OfficeEscape_Source_OfficeEscape_OfficeEscapePlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	OfficeEscape_Source_OfficeEscape_OfficeEscapePlayerController_h_12_RPC_WRAPPERS \
	OfficeEscape_Source_OfficeEscape_OfficeEscapePlayerController_h_12_INCLASS \
	OfficeEscape_Source_OfficeEscape_OfficeEscapePlayerController_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define OfficeEscape_Source_OfficeEscape_OfficeEscapePlayerController_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	OfficeEscape_Source_OfficeEscape_OfficeEscapePlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	OfficeEscape_Source_OfficeEscape_OfficeEscapePlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	OfficeEscape_Source_OfficeEscape_OfficeEscapePlayerController_h_12_INCLASS_NO_PURE_DECLS \
	OfficeEscape_Source_OfficeEscape_OfficeEscapePlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID OfficeEscape_Source_OfficeEscape_OfficeEscapePlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
