// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "OfficeEscapePlayerController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOfficeEscapePlayerController() {}
// Cross Module References
	OFFICEESCAPE_API UClass* Z_Construct_UClass_AOfficeEscapePlayerController_NoRegister();
	OFFICEESCAPE_API UClass* Z_Construct_UClass_AOfficeEscapePlayerController();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	UPackage* Z_Construct_UPackage__Script_OfficeEscape();
// End Cross Module References
	void AOfficeEscapePlayerController::StaticRegisterNativesAOfficeEscapePlayerController()
	{
	}
	UClass* Z_Construct_UClass_AOfficeEscapePlayerController_NoRegister()
	{
		return AOfficeEscapePlayerController::StaticClass();
	}
	UClass* Z_Construct_UClass_AOfficeEscapePlayerController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_APlayerController,
				(UObject* (*)())Z_Construct_UPackage__Script_OfficeEscape,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Collision Rendering Utilities|Transformation" },
				{ "IncludePath", "OfficeEscapePlayerController.h" },
				{ "ModuleRelativePath", "OfficeEscapePlayerController.h" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AOfficeEscapePlayerController>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AOfficeEscapePlayerController::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00800284u,
				nullptr, 0,
				nullptr, 0,
				"Game",
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AOfficeEscapePlayerController, 515039030);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AOfficeEscapePlayerController(Z_Construct_UClass_AOfficeEscapePlayerController, &AOfficeEscapePlayerController::StaticClass, TEXT("/Script/OfficeEscape"), TEXT("AOfficeEscapePlayerController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AOfficeEscapePlayerController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
