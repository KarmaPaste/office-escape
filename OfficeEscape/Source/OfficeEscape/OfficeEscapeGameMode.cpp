// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "OfficeEscapeGameMode.h"
#include "OfficeEscapePlayerController.h"
#include "OfficeEscapeCharacter.h"
#include "UObject/ConstructorHelpers.h"

AOfficeEscapeGameMode::AOfficeEscapeGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AOfficeEscapePlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}